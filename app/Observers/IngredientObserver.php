<?php

namespace App\Observers;

use App\Jobs\NotifyForLowStockIngredientJob;
use App\Models\Ingredient;

class IngredientObserver
{

    public function updated(Ingredient $ingredient)
    {
        if ($ingredient->isDirty('base_stock')) {
            $ingredient->notified = false;
        }

        if ($ingredient->isDirty('stock')) {
            if ($ingredient->stock <= ($ingredient->base_stock / 2) && !$ingredient->notified) {
                $ingredient->notified = true;
                NotifyForLowStockIngredientJob::dispatch($ingredient);
            }
        }
        $ingredient->saveQuietly();
    }
}