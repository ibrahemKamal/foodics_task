<?php

namespace App\Events;

use App\Models\Order;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;

class NewOrderEvent implements ShouldQueue
{
    use Dispatchable;

    /**
     * @var \App\Models\Order
     */
    public Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}