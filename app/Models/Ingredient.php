<?php

namespace App\Models;

use App\Enums\IngredientWeight;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Ingredient extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'stock', 'weight_class', 'base_stock', 'notified'];

    protected $casts = [
        'weight_class' => IngredientWeight::class,
        'notified' => 'bool'
    ];

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class)
            ->using(IngredientProduct::class)
            ->withPivot('weight_class', 'ingredient_value');
    }
}