<?php

namespace App\Models;

use App\Enums\IngredientWeight;
use Illuminate\Database\Eloquent\Relations\Pivot;

class IngredientProduct extends Pivot
{
    public $incrementing = true;

    public $timestamps = false;

    protected $casts = [
        'weight_class' => IngredientWeight::class
    ];
}