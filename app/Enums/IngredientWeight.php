<?php

namespace App\Enums;


enum IngredientWeight: string
{
    case KG = 'kg';
    case G = 'g';
}