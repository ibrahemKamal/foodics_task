<?php
/**
 * OrderService
 *
 * @copyright Copyright © 2022 Robusta Studio. All rights reserved.
 * @author    ibrahem.kamal@robustastudio.com
 */

namespace App\Services;


use App\Enums\IngredientWeightConvertor;
use App\Models\Ingredient;
use App\Models\Product;

class OrderService
{
    /**
     * @var \App\Services\IngredientService
     */
    private IngredientService $ingredientService;

    public function __construct(IngredientService $ingredientService)
    {
        $this->ingredientService = $ingredientService;
    }

    /**
     * check if the product * quantity will have sufficient ingredient
     *
     * @param $product_id
     * @param $quantity
     *
     * @return bool
     */
    public function isStockValid($product_id, $quantity = 1): bool
    {
        $product = Product::with('ingredients')->find($product_id);
        $product->load('ingredients');
        $valid = true;

        $product->ingredients->each(function (Ingredient $ingredient) use ($quantity, &$valid) {
            $required_ingredient_stock =
                $this->ingredientService->convertIngredientWeight(
                    $ingredient->pivot->weight_class,
                    $ingredient->weight_class,
                    $ingredient->pivot->ingredient_value);

            return $valid = ($required_ingredient_stock * $quantity) <= $ingredient->stock;

        });

        return $valid;
    }


}