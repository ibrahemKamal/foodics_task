<?php
/**
 * IngredientWeightConvertor
 *
 * @copyright Copyright © 2022 Robusta Studio. All rights reserved.
 * @author    ibrahem.kamal@robustastudio.com
 */

namespace App\Services;


use App\Enums\IngredientWeight;

class IngredientService
{
    const KG_VALUE = 0.001;
    const G_VALUE = 1;

    public function convertIngredientWeight(IngredientWeight $from, IngredientWeight $to, $value)
    {
        if ($from === $to) {
            return $value;
        }
        return ($this->ingredientWeightMapper($from) * $this->ingredientWeightMapper($to)) * $value;
    }

    protected function ingredientWeightMapper(IngredientWeight $weight): float
    {
        return
            [
                IngredientWeight::KG->value => self::KG_VALUE,
                IngredientWeight::G->value => self::G_VALUE
            ][$weight->value];

    }
}