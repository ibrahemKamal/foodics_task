<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Order */
class OrderResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'total_quantity' => $this->total_quantity,
            'items' => $this->whenLoaded('items', OrderItemResource::collection($this->items)),
            'created_at' => $this->created_at,
        ];
    }
}
