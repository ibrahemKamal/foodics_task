<?php

namespace App\Http\Controllers\Api\Orders;

use App\Events\NewOrderEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Orders\StoreOrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\OrderItem;

class OrderController extends Controller
{
    public function __invoke(StoreOrderRequest $request)
    {
        $data = collect($request->validated('products'));
        $order = Order::create([
            'total_quantity' => $data->sum('quantity')
        ]);
        $order->items()->saveMany($data->map(fn($item) => new OrderItem($item)));
        NewOrderEvent::dispatch($order);
        return response()->json([
            'order'=> new OrderResource($order->fresh('items.product'))
        ]);
    }
}