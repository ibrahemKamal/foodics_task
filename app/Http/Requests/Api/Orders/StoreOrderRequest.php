<?php

namespace App\Http\Requests\Api\Orders;

use App\Rules\ValidStockRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'products' => ['array','required',app(ValidStockRule::class)],
            'products.*.product_id' => 'required|exists:products,id',
            'products.*.quantity' => 'required|numeric|min:1',
        ];
    }



}