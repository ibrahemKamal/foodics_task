<?php

namespace App\Listeners;


use App\Events\NewOrderEvent;
use App\Models\Ingredient;
use App\Services\IngredientService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class UpdateIngredientStockListener implements ShouldQueue
{
    /**
     * @var \App\Services\IngredientService
     */
    public IngredientService $ingredientService;

    public function __construct(IngredientService $ingredientService)
    {
        $this->ingredientService = $ingredientService;
    }

    /**
     * 1- convert all the order ingredients to the base ingredient weight class
     * 2- group all the values by the ingredient id
     * 3-decrease the summed value from the main ingredient stock
     *
     * @param \App\Events\NewOrderEvent $event
     *
     * @return void
     */
    public function handle(NewOrderEvent $event)
    {
        $order = $event->order;
        $order->load('items.product.ingredients');

        $ingredients = collect([]);
        $order->items->each(function ($item) use (&$ingredients) {
            $item->product->ingredients->each(function ($ingredient) use (&$ingredients) {

                $used_ingredient_stock = $this->ingredientService->convertIngredientWeight(
                    $ingredient->pivot->weight_class,
                    $ingredient->weight_class,
                    $ingredient->pivot->ingredient_value);

                $ingredients->push(['id' => $ingredient->id, 'stock' => $used_ingredient_stock]);
            });
        });
        $ingredients->groupBy('id')
            ->map(function ($ingredient, $ingredient_id) {
                return ['ingredient_id' => $ingredient_id, 'total_order_stock_weight' => $ingredient->sum('stock')];
            })
            ->each(function ($ingredient) {
                Ingredient::find($ingredient['ingredient_id'])
                    ->decrement('stock', $ingredient['total_order_stock_weight']);
            });

    }
}