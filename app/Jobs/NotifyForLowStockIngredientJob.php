<?php

namespace App\Jobs;

use App\Mail\LowStockIngredientMail;
use App\Models\Ingredient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class NotifyForLowStockIngredientJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\Models\Ingredient
     */
    public Ingredient $ingredient;

    public function __construct(Ingredient $ingredient)
    {
        $this->ingredient = $ingredient;
    }

    public function handle()
    {
        // in real life application this would read emails from db
        Mail::to('test@test.com')
            ->send(new LowStockIngredientMail($this->ingredient));
    }
}