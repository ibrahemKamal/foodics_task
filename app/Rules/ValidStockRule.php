<?php

namespace App\Rules;

use App\Services\OrderService;
use Illuminate\Contracts\Validation\InvokableRule;

class ValidStockRule implements InvokableRule
{
    /**
     * @var \App\Services\OrderService
     */
    private OrderService $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param \Closure $fail
     *
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        foreach ($value as $item) {
            if (!$this->orderService->isStockValid($item['product_id'], $item['quantity'])) {
                $fail("The :attribute with id {$item['product_id']} has insufficient ingredient stock");
                break;
            }
        }
    }
}