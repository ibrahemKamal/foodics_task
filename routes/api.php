<?php

use App\Http\Controllers\Api\Orders\OrderController;
use Illuminate\Support\Facades\Route;


Route::post('order', OrderController::class)
->name('make.order');