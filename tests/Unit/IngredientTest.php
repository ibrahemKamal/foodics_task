<?php

namespace Tests\Unit;


use App\Enums\IngredientWeight;
use App\Models\Ingredient;
use App\Models\Product;
use App\Services\IngredientService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class IngredientTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var \App\Services\IngredientService|mixed
     */
    private mixed $ingredientService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->ingredientService = $this->app->make(IngredientService::class);
    }

    public function test_ingredient_stock_is_updated_after_success_order()
    {
        $ingredient = Ingredient::factory([
                'stock' => 10
            ]
        )->create();
        $product = Product::factory()
            ->hasAttached($ingredient, [
                'ingredient_value' => 100
            ])->create();

        $reduced_stock_quantity = $this->ingredientService->convertIngredientWeight(
            $product->ingredients()->first()->pivot->weight_class,
            $ingredient->weight_class,
            $product->ingredients()->first()->pivot->ingredient_value,
        );

        $payload = [
            'products' => [
                [
                    'product_id' => $product->id,
                    'quantity' => 1
                ]
            ]
        ];

        $this->postJson(route('make.order'), $payload);

        $this->assertDatabaseHas('ingredients', [
            'id' => $ingredient->id,
            'stock' => $ingredient->stock - $reduced_stock_quantity
        ]);

    }


    public function test_suppliers_gets_informed_when_ingredient_stock_below_half()
    {
        $ingredient = Ingredient::factory([
                'stock' => 10,
                'base_stock' => 10,
                'notified' => false
            ]
        )->create();
        $product = Product::factory()
            ->hasAttached($ingredient, [
                'ingredient_value' => 5,
                'weight_class' => IngredientWeight::KG->value
            ])->create();

        $payload = [
            'products' => [
                [
                    'product_id' => $product->id,
                    'quantity' => 1
                ]
            ]
        ];
        $this->assertDatabaseHas('ingredients', ['id' => $ingredient->id, 'notified' => false]);

        $this->postJson(route('make.order'), $payload);

        $this->assertDatabaseHas('ingredients', ['id' => $ingredient->id, 'notified' => true]);

    }
}