<?php

namespace Tests\Unit;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    public function test_order_with_insufficient_stock()
    {
        $payload = [
            'products' => [
                [
                    'product_id' => Product::first()->id,
                    'quantity' => 100
                ]
            ]
        ];
        $this->postJson(route('make.order'), $payload)
            ->assertUnprocessable()
            ->assertJsonValidationErrorFor('products');
    }

    public function test_orders_stored_successfully()
    {
        $payload = [
            'products' => [
                [
                    'product_id' => Product::first()->id,
                    'quantity' => 1
                ]
            ]
        ];

        $this->postJson(route('make.order'), $payload)
            ->assertSuccessful();
        $this->assertDatabaseCount('orders', 1);
    }

    public function test_created_orders_return_object()
    {
        $payload = [
            'products' => [
                [
                    'product_id' => Product::first()->id,
                    'quantity' => 1
                ]
            ]
        ];

        $this->postJson(route('make.order'), $payload)
            ->assertJsonStructure([
                'order' => [
                    'id',
                    'total_quantity',
                    'items',
                    'created_at',
                ]
            ]);
    }
}