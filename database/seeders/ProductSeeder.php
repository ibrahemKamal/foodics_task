<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    public function run()
    {
        $ingredients = Ingredient::get('id');
        Product::factory(3)
            ->hasAttached(
                $ingredients, [
                    'ingredient_value' => 150
                ]
            )->create();
    }
}
