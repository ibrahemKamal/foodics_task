<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    public function run()
    {
        $ingredients = collect([
            [
                'name' => 'Beef',
                'stock' => '20',
                'base_stock' => '20',
            ],
            [
                'name' => 'Cheese',
                'stock' => '5',
                'base_stock' => '5',
            ],
            [
                'name' => 'Onion',
                'stock' => '1',
                'base_stock' => '1',
            ],
        ])
            ->map(function ($ingredient) {
                $ingredient['created_at'] = now();
                $ingredient['updated_at'] = now();
                return $ingredient;
            })->toArray();

        Ingredient::insert($ingredients);
    }
}
