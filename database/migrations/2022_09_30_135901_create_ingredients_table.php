<?php

use App\Enums\IngredientWeight;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientsTable extends Migration
{
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->id();

            $table->string('name');

            $table->double('stock');

            $table->double('base_stock');

            $table->string('weight_class')->default(IngredientWeight::KG->value);

            $table->boolean('notified')->default(false);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ingredients');
    }
}