<?php

use App\Enums\IngredientWeight;
use App\Models\Ingredient;
use App\Models\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientProductTable extends Migration
{
    public function up()
    {
        Schema::create('ingredient_product', function (Blueprint $table) {
            $table->id();

            $table->foreignIdFor(Ingredient::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->foreignIdFor(Product::class)
                ->constrained()
                ->cascadeOnDelete();

            $table->string('weight_class')->default(IngredientWeight::G->value);

            $table->double('ingredient_value');

        });
    }

    public function down()
    {
        Schema::dropIfExists('ingredient_product');
    }
}