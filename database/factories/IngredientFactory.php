<?php

namespace Database\Factories;

use App\Enums\IngredientWeight;
use App\Models\Ingredient;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class IngredientFactory extends Factory
{
    protected $model = Ingredient::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'stock' => $this->faker->randomFloat(),
            'base_stock' => $this->faker->randomFloat(),
            'weight_class' => IngredientWeight::KG,
            'notified' => $this->faker->boolean(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
